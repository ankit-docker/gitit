# Dockerfile for Gitit
#
# Copyright 2020, Ankit R Gadiya
# BSD 3-Clause License
FROM debian:buster-slim
LABEL MAINTAINER="Ankit R Gadiya git@argp.in"

RUN apt update && \
  apt install git gitit -y

WORKDIR /wiki

EXPOSE 80
ENTRYPOINT ["/usr/bin/gitit", "-p", "80", "-l", "0.0.0.0", "-f", "my.conf"]
