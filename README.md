# Gitit

## Usage

Run Podman container for Gitit.

```bash
$ podman run \
  --detach \
  --name gitit \
  --volume `pwd`/wiki:/wiki \
  --port 30004:80 \
  --restart always \
  registry.gitlab.com/ankit-docker/gitit/gitit:latest
```
